import requests
import json
import urllib.request

def send(person):

    person['name'] = person['second_name'] + ' ' + person['first_name']
    person.pop(person['second_name'], None)
    person.pop(person['first_name'], None)

    person['number'] = person.pop('phone', None)
    person['vacancy'] = person.pop('position', None)
    person['birthday'] = person.pop('birthdate', None)
    person['proskills'] = person.pop('pro_skills', None)

    if not person['experience']:
        person['experience'] = None

    # body = {
	#     "name": person['second_name'] + ' ' + person['first_name'],
    #     "address": person['address'],
    #     "email": person['email'],
    #     "number": person['phone'],
    #     "birthday": person['birthdate'],
    #     "vacancy": person['position'],
    #     # "employment": "Полный день, гибкий график",
    # #     ],
    #     "proskills": [
    #         {
    #             "skillset": person['pro_skills']['skillset'],
    #         "skills": [
    #           "Python",
    #           "C++",
    #           "Javascript"
    #         ]
    #       },
    #       {
    #         "skillset": "Фреймворки и технологии",
    #         "skills": [
    #           "React/Redux",
    #           "MEAN",
    #           "Django",
    #           "Arduino"
    #         ]
    #       },
    #       {
    #         "skillset": "Монтаж",
    #         "skills": [
    #           "Монтаж апаратуры",
    #           "Прокладка кабеля"
    #         ]
    #       }
    #     ],
    #     "experience": [
    #       {
    #         "company": "ГКБ 7",
    #         "start": "2013",
    #         "end": "2013",
    #         "role": "Дворник"
    #       },
    #       {
    #         "company": "Ростелеком",
    #         "start": "Июнь 2014",
    #         "end": "Июль 2014",
    #         "role": "Менеджер по продажам"
    #       },
    #       {
    #         "company": "РА Лариса",
    #         "start": "Февраль 2017",
    #         "end": "Июль 2017",
    #         "role": "Офис-менеджер"
    #       }
    #     ],
    #     "about": "Один из 7 миллиардов людей на земле."
    # }

    # body = {
    #     'second_name': 'Мищагин',
    #     'first_name': 'Тимур',
    #     'birthdate': '3-12-1999',
    #     'email': 'aslkdfn@asdfkas.com',
    #     'phone': '12341234',
    #     'address': 'казнь 3',
    #     'education': [
    #         {
    #             'school': 'школа 3',
    #             'start': '2017',
    #             'finish': '1234',
    #             'degree': 'средний',
    #             'role': 'Программист'
    #         }
    #     ],
    #     'position': 'Программист',
    #     'experience': [
    #         {'company': 'пятерочка', 'start': '20123'}
    #         ],
    #         'pro_skills': [
    #             {
    #                 'skillset': 'MS',
    #                 'skills': [
    #                     'Word', 'Excel'
    #                     ]
    #             }
    #         ]
    #     }


    url = 'https://script.google.com/macros/s/AKfycbyXKz7Om1mnpcvclC2GIkfJ6-FAGJcD8lSb-KoLQ6xsjEJj2KXm/exec'

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url, data=json.dumps(person), headers=headers)
    # json_obj = {
    #     'name': person['second_name'] + ' ' + person['first_name'],
    #     'birthdate': person['birthdate'].strftime('%d/%m/%Y'),
    #     'email': person['email'],
    #     'phone': person['phone'],
    #     'address': person['address']
    # }

    return r.text

print(send(1).text)