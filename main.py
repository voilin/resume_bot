import telebot
from keyboard import *
import time
import datetime
from validate_email import validate_email
import send_person

bot = telebot.TeleBot("604898274:AAFgehdy6WhUb5jhbd8iJDTtRMYB_iwaX0M")

@bot.message_handler(commands=['start'])
def start_handler(m):
    cid = m.chat.id
    bot.send_message(cid, 'Здравствуйте', reply_markup=first_markup)

@bot.message_handler(commands=['help'])
def help_handler(m):
    cid = m.chat.id
    bot.send_message(cid, 'Информация о нас', reply_markup=first_markup)

@bot.message_handler(func=lambda m: m.text == 'Информация')
def information_handler(m):
    cid = m.chat.id
    bot.send_message(cid, 'Информация о нас')

@bot.message_handler(func=lambda m: m.text == 'Создать резюме')
def start_resume_handler(m):
    cid = m.chat.id
    bot.send_message(cid, 'Заполнение резюме')
    bot.send_message(cid, 'Введите свою фамилию', reply_markup=start_resume_quit_markup)
    bot.register_next_step_handler(m, second_name_handler)

def second_name_handler(m):
    cid = m.chat.id
    if m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:
        person = {}
        person['second_name'] = m.text.strip().capitalize()
        bot.send_message(cid, 'Введите своё имя', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, first_name_handler, person)

def first_name_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(cid, 'Введите свою фамилию', reply_markup=start_resume_quit_markup)
        bot.register_next_step_handler(m, second_name_handler)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        person['first_name'] = m.text.strip().capitalize()
        bot.send_message(
            cid,
            'Отлично, теперь введите вашу дату рождения в формате дд-мм-гггг\nПримеры: <b>3-1-1999</b>, <b>08-12-1989</b>',
            parse_mode='HTML',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, birthdate_handler, person)


def birthdate_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(cid, 'Введите своё имя', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, first_name_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        date = m.text.strip()

        try:
            birthdate = datetime.datetime.strptime(date, "%d-%m-%Y")
            person['birthdate'] = m.text
            bot.send_message(cid, 'Введите ваш email', reply_markup=back_step_markup)
            bot.register_next_step_handler(m, email_handler, person)
        except ValueError:
            bot.send_message(
                cid,
                'Неверный формат данных, необходимо вводить дату в формате дд-мм-гггг. Повторите ещё раз',
                reply_markup=back_step_markup
            )
            bot.register_next_step_handler(m, birthdate_handler, person)

def email_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите вашу дату рождения в формате дд-мм-гггг\nПримеры: <b>3-1-1999</b>, <b>08-12-1989</b>',
            parse_mode='HTML',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, birthdate_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        email = m.text

        if validate_email(email):
            person['email'] = email.strip()
            bot.send_message(cid, 'Введите ваш телефон', reply_markup=back_step_markup)
            bot.register_next_step_handler(m, phone_handler, person)

        else:
            bot.send_message(cid, 'Похоже вы неверно ввели email адрес, попробуйте ещё раз')
            bot.register_next_step_handler(m, email_handler, person)

def phone_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите ваш email',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, email_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:


        phone = m.text.lower()
        person['phone'] = phone
        bot.send_message(cid, 'Введите ваш адрес, он будет указан также, как вы его отправите', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, address_handler, person)

def address_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите ваш телефон',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, phone_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        address = m.text.strip()
        person['address'] = address
        person['education'] = []
        bot.send_message(cid, 'Теперь перейдём к заполнению образования')
        bot.send_chat_action(cid, 'typing')
        time.sleep(0.5)
        bot.send_message(cid, 'Введите ваше образовательное учреждение', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, school_handler, person)

def school_handler(m, person):
    cid = m.chat.id
    person['education'].append({})
    if m.text == back:
        bot.send_message(
            cid,
            'Введите ваш адрес',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, address_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        school = m.text.strip()
        person['education'][len(person['education']) - 1]['school'] = school

        bot.send_message(cid, 'Введите год начала обучения')
        bot.register_next_step_handler(m, start_date_school_handler, person)

def start_date_school_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите вашу школу',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, school_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        if m.text.isdigit():
            person['education'][len(person['education']) - 1]['start'] = m.text
            bot.send_message(cid, 'Теперь введите год окончания обучения', reply_markup=back_step_markup)
            bot.register_next_step_handler(m, finish_date_school_handler, person)
        else:

            bot.send_message(cid, 'Неверное значение года, попробуйте ещё раз.', reply_markup=back_step_markup)
            bot.register_next_step_handler(m, start_date_school_handler, person)

def finish_date_school_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите год начала обучения',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, school_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        if m.text.isdigit():
            person['education'][len(person['education']) - 1]['end'] = m.text
            bot.send_message(cid, 'Теперь введите уровень образования (средний, высший, дополнительный)', reply_markup=back_step_markup)
            bot.register_next_step_handler(m, school_degree_handler, person)
        else:

            bot.send_message(cid, 'Неверное значение года, попробуйте ещё раз.', reply_markup=back_step_markup)
            bot.register_next_step_handler(m, start_date_school_handler, person)

def school_degree_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите год конца обучения',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, school_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        after_education_handler = telebot.types.ReplyKeyboardMarkup(True, True)
        after_education_handler.row(add_edu, end_input)
        after_education_handler.row(back, quit)

        degree = m.text.strip()
        person['education'][len(person['education']) - 1]['degree'] = degree
        bot.send_message(cid, str(person), reply_markup=after_education_handler)
        bot.send_message(cid, 'Мы можем закончить ввод образования либо продолжить дальше.')
        bot.register_next_step_handler(m, education_handler, person)

def education_handler(m, person):
    cid = m.chat.id
    if m.text == add_edu:
        bot.send_message(cid, 'Введите учебное заведение', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, school_handler, person)
    elif m.text == back:
        bot.send_message(
            cid,
            'Введите уровень образования (средний, высший, дополнительный)',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, school_degree_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:
        bot.send_message(cid, 'Введите название желаемой позиции', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, position_handler, person)

def position_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите уровень образования (средний, высший, дополнительный)',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, school_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        position = m.text.strip()
        person['position'] = position

        bot.send_message(cid, 'Теперь ввёдём ваш опыт работы')
        bot.send_chat_action(cid, 'typing')
        time.sleep(0.5)
        bot.send_message(cid, 'Введите место трудоустройства либо пропустите ввод опыта работы.', reply_markup=work_exp_markup)
        bot.register_next_step_handler(m, exp_handler, person)

def exp_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите название желаемой позиции',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, school_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    elif m.text == skip:
        pass
    else:
        person['experience'] = []
        person['experience'].append({})

        person['experience'][len(person['experience']) - 1]['company'] = m.text.strip()

        bot.send_message(cid, 'Введите дату начала работы, в документе будет написано также', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, start_work_handler, person)

def start_work_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите место вашего трудоустройства',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, school_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        if m.text:
            person['experience'][len(person['experience']) - 1]['start'] = m.text
            bot.send_message(cid, 'Теперь дату окончания работы, в документе будет написано также', reply_markup=back_step_markup)
            bot.register_next_step_handler(m, finish_work_handler, person)

def finish_work_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите дату начала работы, в документе будет написано также',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, start_work_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:

        person['education'][len(person['education']) - 1]['end'] = m.text
        bot.send_message(cid, 'Теперь введите должность', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, role_handler, person)

def role_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите дату окончания работы',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, finish_work_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    else:
        person['education'][len(person['education']) - 1]['role'] = m.text.strip()
        bot.send_message(cid,
        """Добавьте дополнительное место трудоустройства либо перейдите к заполнению профессиональных способностей.
        Пример: Microsoft Office, и введите названия навыков: Excel, Word""",
        reply_markup=after_work_exp_markup)
        bot.register_next_step_handler(m, skill_set_handler, person)

def skill_set_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите вашу должность',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, role_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    elif m.text == add_work:
        bot.send_message(cid, 'Введите место вашего трудоустройства', reply_markup=back_step_markup)
        bot.register_next_step_handler(m, exp_handler, person)
    else:
        # bot.send_message(cid, str(person))
        person['pro_skills'] = []
        person['pro_skills'].append({})
        person['pro_skills'][len(person['pro_skills']) - 1]['skillset'] = m.text.strip()
        person['pro_skills'][len(person['pro_skills']) - 1]['skills'] = []
        bot.send_message(cid, 'Теперь добавьте навык', reply_markup=skill_set_markup)
        bot.register_next_step_handler(m, skill_handler, person)

def skill_handler(m, person):
    cid = m.chat.id
    if m.text == back:
        bot.send_message(
            cid,
            'Введите название набора навыков',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, skill_set_handler, person)
    elif m.text == quit:
        bot.send_message(cid, 'Вы больше не заполняете резюме', reply_markup=first_markup)
        bot.clear_step_handler(m)
    elif m.text == add_set:
        bot.send_message(
            cid,
            'Введите название набора навыков',
            reply_markup=back_step_markup
        )
        bot.register_next_step_handler(m, skill_set_handler, person)
    elif m.text == exit_skills:
        bot.send_chat_action(cid, 'typing')
        url = send_person.send(person)
        bot.send_message(cid, 'Ваше резюме достпупно ' + url)
        # bot.send_message(cid, 'Отлично, пришло время заполнить опыт работы. Введите место прошлой', reply_markup=back_step_markup)
        # bot.register_next_step_handler(m, expirience_handler, person)
    else:
        person['pro_skills'][len(person['pro_skills']) - 1]['skills'].append(m.text.strip())
        bot.send_message(cid, 'Продолжите добавлять навыки, либо выберите действие с клавиатуры', reply_markup=skill_set_markup)
        bot.register_next_step_handler(m, skill_handler, person)


bot.polling(none_stop=True)