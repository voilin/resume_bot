import telebot

first_markup = telebot.types.ReplyKeyboardMarkup(True, True)
first_markup.row(
    'Создать резюме',
    'Информация'
    )

end_input = 'Закончить ввод образования'
add_edu = 'Добавить ещё одно учебное заведение'
back = 'Вернуться назад'
quit = 'Выйти'

back_step_markup = telebot.types.ReplyKeyboardMarkup(True, True)
back_step_markup.row(back, quit)

start_resume_quit_markup = telebot.types.ReplyKeyboardMarkup(True, True)
start_resume_quit_markup.row(quit)

skip = 'Пропустить'
work_exp_markup = telebot.types.ReplyKeyboardMarkup(True, True)
work_exp_markup.row(skip)
work_exp_markup.row(back, quit)

add_work = 'Добавить ещё одно трудоустройство'
after_work_exp_markup = telebot.types.ReplyKeyboardMarkup(True, True)
after_work_exp_markup.row(add_work)
after_work_exp_markup.row(back, quit)

add_set = 'Добавить новый набор навыков'
exit_skills = 'Закончить ввод навыков'
skill_set_markup = telebot.types.ReplyKeyboardMarkup(True, True)
skill_set_markup.row(add_set)
skill_set_markup.row(exit_skills)
skill_set_markup.row(back, quit)